﻿namespace Interpolation
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.box_Xo = new System.Windows.Forms.TextBox();
            this.box_Yo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.box_Yi = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.box_Xi = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.box_theta = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.box_delta = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.box_T = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.box_Vmax = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.box_Vi = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.runButton = new System.Windows.Forms.Button();
            this.box_a = new System.Windows.Forms.NumericUpDown();
            this.box_a2 = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.box_a)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.box_a2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("SimSun", 12F);
            this.label1.Location = new System.Drawing.Point(193, 73);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Xo (m)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // box_Xo
            // 
            this.box_Xo.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_Xo.Location = new System.Drawing.Point(310, 71);
            this.box_Xo.Margin = new System.Windows.Forms.Padding(2);
            this.box_Xo.Name = "box_Xo";
            this.box_Xo.Size = new System.Drawing.Size(83, 26);
            this.box_Xo.TabIndex = 1;
            this.box_Xo.Text = "0.1";
            this.box_Xo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // box_Yo
            // 
            this.box_Yo.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_Yo.Location = new System.Drawing.Point(310, 123);
            this.box_Yo.Margin = new System.Windows.Forms.Padding(2);
            this.box_Yo.Name = "box_Yo";
            this.box_Yo.Size = new System.Drawing.Size(83, 26);
            this.box_Yo.TabIndex = 4;
            this.box_Yo.Text = "0.1";
            this.box_Yo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("SimSun", 12F);
            this.label2.Location = new System.Drawing.Point(193, 125);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Yo (m)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // box_Yi
            // 
            this.box_Yi.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_Yi.Location = new System.Drawing.Point(310, 224);
            this.box_Yi.Margin = new System.Windows.Forms.Padding(2);
            this.box_Yi.Name = "box_Yi";
            this.box_Yi.Size = new System.Drawing.Size(83, 26);
            this.box_Yi.TabIndex = 8;
            this.box_Yi.Text = "-0.11";
            this.box_Yi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("SimSun", 12F);
            this.label3.Location = new System.Drawing.Point(193, 226);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Yi (m)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // box_Xi
            // 
            this.box_Xi.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_Xi.Location = new System.Drawing.Point(310, 172);
            this.box_Xi.Margin = new System.Windows.Forms.Padding(2);
            this.box_Xi.Name = "box_Xi";
            this.box_Xi.Size = new System.Drawing.Size(83, 26);
            this.box_Xi.TabIndex = 6;
            this.box_Xi.Text = "0.11";
            this.box_Xi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("SimSun", 12F);
            this.label4.Location = new System.Drawing.Point(193, 174);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Xi (m)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("SimSun", 12F);
            this.label5.Location = new System.Drawing.Point(193, 374);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "a2 (m/s^2)";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("SimSun", 12F);
            this.label6.Location = new System.Drawing.Point(193, 322);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 13;
            this.label6.Text = "a (m/s^2)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // box_theta
            // 
            this.box_theta.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_theta.Location = new System.Drawing.Point(310, 271);
            this.box_theta.Margin = new System.Windows.Forms.Padding(2);
            this.box_theta.Name = "box_theta";
            this.box_theta.Size = new System.Drawing.Size(83, 26);
            this.box_theta.TabIndex = 12;
            this.box_theta.Text = "-200";
            this.box_theta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("SimSun", 12F);
            this.label7.Location = new System.Drawing.Point(193, 273);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "theta (deg)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // box_delta
            // 
            this.box_delta.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_delta.Location = new System.Drawing.Point(310, 574);
            this.box_delta.Margin = new System.Windows.Forms.Padding(2);
            this.box_delta.Name = "box_delta";
            this.box_delta.Size = new System.Drawing.Size(83, 26);
            this.box_delta.TabIndex = 24;
            this.box_delta.Text = "0.00001";
            this.box_delta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("SimSun", 12F);
            this.label9.Location = new System.Drawing.Point(193, 576);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 23;
            this.label9.Text = "delta (m)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // box_T
            // 
            this.box_T.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_T.Location = new System.Drawing.Point(310, 522);
            this.box_T.Margin = new System.Windows.Forms.Padding(2);
            this.box_T.Name = "box_T";
            this.box_T.Size = new System.Drawing.Size(83, 26);
            this.box_T.TabIndex = 22;
            this.box_T.Text = "0.001";
            this.box_T.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("SimSun", 12F);
            this.label10.Location = new System.Drawing.Point(193, 524);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 16);
            this.label10.TabIndex = 21;
            this.label10.Text = "T (s)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // box_Vmax
            // 
            this.box_Vmax.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_Vmax.Location = new System.Drawing.Point(310, 473);
            this.box_Vmax.Margin = new System.Windows.Forms.Padding(2);
            this.box_Vmax.Name = "box_Vmax";
            this.box_Vmax.Size = new System.Drawing.Size(83, 26);
            this.box_Vmax.TabIndex = 20;
            this.box_Vmax.Text = "0.5";
            this.box_Vmax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("SimSun", 12F);
            this.label11.Location = new System.Drawing.Point(193, 475);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 16);
            this.label11.TabIndex = 19;
            this.label11.Text = "Vmax (m/s)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // box_Vi
            // 
            this.box_Vi.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_Vi.Location = new System.Drawing.Point(310, 421);
            this.box_Vi.Margin = new System.Windows.Forms.Padding(2);
            this.box_Vi.Name = "box_Vi";
            this.box_Vi.Size = new System.Drawing.Size(83, 26);
            this.box_Vi.TabIndex = 18;
            this.box_Vi.Text = "0.1";
            this.box_Vi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("SimSun", 12F);
            this.label12.Location = new System.Drawing.Point(193, 423);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 16);
            this.label12.TabIndex = 17;
            this.label12.Text = "Vi (m/s)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // runButton
            // 
            this.runButton.Font = new System.Drawing.Font("SimSun", 14F);
            this.runButton.Location = new System.Drawing.Point(218, 640);
            this.runButton.Margin = new System.Windows.Forms.Padding(2);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(150, 40);
            this.runButton.TabIndex = 25;
            this.runButton.Text = "运行";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.button_Click);
            // 
            // box_a
            // 
            this.box_a.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_a.Location = new System.Drawing.Point(310, 321);
            this.box_a.Margin = new System.Windows.Forms.Padding(2);
            this.box_a.Name = "box_a";
            this.box_a.Size = new System.Drawing.Size(82, 26);
            this.box_a.TabIndex = 26;
            this.box_a.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.box_a.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // box_a2
            // 
            this.box_a2.Font = new System.Drawing.Font("SimSun", 12F);
            this.box_a2.Location = new System.Drawing.Point(310, 373);
            this.box_a2.Margin = new System.Windows.Forms.Padding(2);
            this.box_a2.Name = "box_a2";
            this.box_a2.Size = new System.Drawing.Size(82, 26);
            this.box_a2.TabIndex = 27;
            this.box_a2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.box_a2.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("SimSun", 18F);
            this.label8.Location = new System.Drawing.Point(214, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 24);
            this.label8.TabIndex = 28;
            this.label8.Text = "平面圆弧插补";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 708);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.box_a2);
            this.Controls.Add(this.box_a);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.box_delta);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.box_T);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.box_Vmax);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.box_Vi);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.box_theta);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.box_Yi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.box_Xi);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.box_Yo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.box_Xo);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.box_a)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.box_a2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox box_Xo;
        private System.Windows.Forms.TextBox box_Yo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox box_Yi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox box_Xi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox box_theta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox box_delta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox box_T;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox box_Vmax;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox box_Vi;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.NumericUpDown box_a;
        private System.Windows.Forms.NumericUpDown box_a2;
        private System.Windows.Forms.Label label8;
    }
}

