﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace Interpolation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button_Click(object sender, EventArgs e)
        {
            runButton.Text = "运行中...";
            double Xo = double.Parse(box_Xo.Text);
            double Yo = double.Parse(box_Yo.Text);
            double Xi = double.Parse(box_Xi.Text);
            double Yi = double.Parse(box_Yi.Text);
            double d_theta_total = double.Parse(box_theta.Text);
            double a = (double) box_a.Value;
            double a2 = (double) box_a2.Value;
            double Vi = double.Parse(box_Vi.Text);
            double Vmax = double.Parse(box_Vmax.Text);
            double T = double.Parse(box_T.Text);
            double delta = double.Parse(box_delta.Text);
            MessageBox.Show("Entering calculation...");
            int cycles = path_planning(Xo, Yo, Xi, Yi, d_theta_total, a, a2, Vi, Vmax, T, delta);
            MessageBox.Show("Finished calculation! Total of " + cycles + " cycles.");
            runButton.Text = "运行";
        }

        private int path_planning(double Xo, double Yo, double Xi, double Yi, double d_theta_total, double a, double a2, double Vi, double Vmax, double T, double delta)
        {
            // 查看是否数据合法
            if (Vmax*T/delta > 640)
            {
                MessageBox.Show("组合不合法。Vmax*T/delta 必须小于640。");
                return 0;
            }

            // 处理角度
            d_theta_total = d_theta_total / 180 * Math.PI;

            // 处理轨迹方向
            int sign;
            if (d_theta_total < 0)
                sign = -1;
            else
                sign = 1;

            // 处理起始点
            double R = Math.Sqrt(Math.Pow((Xi - Xo), 2) + Math.Pow((Yi - Yo), 2));
            double Xin = Xi - Xo;
            double Yin = Yi - Yo;

            // 处理终点角度
            double theta_i;
            if (Xin > 0)
                theta_i = Math.Atan(Yin / Xin);
            else
                theta_i = Math.Atan(Yin / Xin) + Math.PI;
            double theta_s = theta_i;
            double theta_e_total = theta_s + d_theta_total;

            // 换算最大速度到最大角速度
            double w_max = Vmax / R;

            // 换算起始速度到起始角速度
            double w_i = Vi / R;

            // 主循环
            int i = 0;
            int phase = 1;
            double w_s = w_i, w_e = 0;
            double Xs = Xi, Ys = Yi, Xe, Ye;
            double t;
            double d_theta, theta_e, theta_brake;
            while (!(w_s * R < 0.5 * a2 * T && (phase == 3 || phase == 4)))
            {
                //  判断是否该减速
                t = w_s / (a2 / R);
                if (phase < 3 && a2 < a)
                    theta_brake = theta_e_total - sign * (0.5 * (a2 / R) * t * t) - 0.5 * a / a2 * sign * (w_s * T);
                else
                    theta_brake = theta_e_total - sign * (0.5 * (a2 / R) * t * t) - sign * (w_s * T);


                // 判断是在哪个阶段
                if (phase == 1 && w_s + (a / R) * T > w_max)
                    phase = 2;
                if (phase != 3 && (sign * (theta_brake - theta_s) <= 0))
                    phase = 3;
                if (phase == 3 && sign * (theta_brake - theta_s) > 0)
                    phase = 4;

                // 判断终角速度
                switch (phase) {
                    case 1:
                        w_e = w_s + (a / R) * T;
                        break;
                    case 2:
                        w_e = w_max;
                        break;
                    case 3:
                        w_e = w_s - (a2 / R) * T;
                        if (Math.Abs(w_e) < Math.Pow(0.1, 14) && sign * (theta_s + sign * (w_s + w_e) * T/2 - theta_e_total) <= 0) { w_e = w_s; }
                        break;
                    case 4:
                        w_e = w_s;
                        break;
                    default:
                        break;
                }

                // 算取角度变化
                d_theta = (w_s + w_e) * T / 2;

                // 算取终角度
                theta_e = theta_s + sign * d_theta;

                // 得到下一个目标点
                Xe = R * Math.Cos(theta_e) + Xo;
                Ye = R * Math.Sin(theta_e) + Yo;

                // 换成协议格式信息
                Protocol Xproto = new Protocol(Xs, Xe, delta, "00");
                Protocol Yproto = new Protocol(Ys, Ye, delta, "01");

                // 得到协议值
                int xPSC = Xproto.getPSC();
                int xARR = Xproto.getARR();
                string xAxis = Xproto.getAXIS();
                int xDIR = Xproto.getDIR();
                int xS = Xproto.getS();

                int yPSC = Yproto.getPSC();
                int yARR = Yproto.getARR();
                string yAxis = Yproto.getAXIS();
                int yDIR = Yproto.getDIR();
                int yS = Yproto.getS();

                // 计算真实终点
                int xsign, ysign, xS_real, yS_real;
                if (xDIR == 1)
                    xsign = 1;
                else
                    xsign = -1;
                if (yDIR == 1)
                    ysign = 1;
                else
                    ysign = -1;

                if (xS != 0)
                    xS_real = (int) Math.Floor(Math.Floor(1.0 * 108_000 / xPSC) / xARR);
                else
                    xS_real = 0;
                if (yS != 0)
                    yS_real = (int)Math.Floor(Math.Floor(1.0 * 108_000 / yPSC) / yARR);
                else
                    yS_real = 0;

                Xe = Xs + xsign * delta * xS_real;
                Ye = Ys + ysign * delta * yS_real;

                // 设置终角速度与终角度为下一循环的起角速度与起角度
                w_s = w_e;
                theta_s = theta_e;
                Xs = Xe;
                Ys = Ye;

                //Thread.Sleep(1);
                i++;
            }
            MessageBox.Show("End point = (" + Xs + ", " + Ys + ")");
            return i;
        }
    }
}
