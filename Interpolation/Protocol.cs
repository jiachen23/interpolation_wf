﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpolation
{
    class Protocol
    {
        private int PSC;
        private int ARR;
        private string AXIS;
        private int DIR;
        private int S;

        // Constructor
        public Protocol(double Xs, double Xe, double delta, string Axis)
        {
            // 按照每微妙的起点终点, 计算并返回协议化的数据
            int pulse = 108_000;
            double dx = Xe - Xs;
            double dxn = Math.Abs(dx);
            int x_pulse_num = (int)Math.Floor(dxn / delta);

            // 查找x的分频值PSC和重载值ARR
            if (x_pulse_num == 0)
            {
                PSC = 0;
                ARR = 0;
            }
            else
            {
                double x_factor = pulse / x_pulse_num;
                int i_psc = 0;
                while (x_factor > 100)
                {
                    x_factor = x_factor / 2;
                    i_psc = i_psc + 1;
                }
                PSC = (int)Math.Pow(2, i_psc);
                ARR = (int)Math.Floor(x_factor);
            }

            // 设置轴AXIS
            AXIS = Axis;

            // 设置方向DIR
            if (dx >= 0)
                DIR = 1;
            else
                DIR = 0;

            // 设置脉冲数Ｓ
            if (x_pulse_num == 0)
                S = 0;
            else
                S = (int)Math.Floor(Math.Floor(1.0*pulse/PSC)/ARR);
        }

        // Getters
        public int getPSC() { return PSC; }
        public int getARR() { return ARR; }
        public string getAXIS() { return AXIS; }
        public int getDIR() { return DIR; }
        public int getS() { return S; }
    }
}
